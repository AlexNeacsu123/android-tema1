package com.example.tema1.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.util.Log;

import com.example.tema1.R;
import com.example.tema1.fragments.F1A2;
import com.example.tema1.fragments.F2A2;
import com.example.tema1.fragments.F3A2;
import com.example.tema1.interfaces.ActivityFragmentCommunication;

public class MainActivity2 extends AppCompatActivity implements ActivityFragmentCommunication {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        addF1A2();
    }

    public void addF1A2()
    {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        String tag = F1A2.class.getName();
        FragmentTransaction addTransaction = transaction.add(
                R.id.frame_layout, F1A2.newInstance("",""), tag
        );

        addTransaction.commit();
    }

    @Override
    public void openSecondActivity() {

    }

    @Override
    public void replaceWithF3A2() {

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        String tag = F3A2.class.getName();
        FragmentTransaction replaceTransaction = transaction.replace(
                R.id.frame_layout, F3A2.newInstance("",""), tag
        );

        replaceTransaction.addToBackStack(tag);
        replaceTransaction.commit();
    }

    @Override
    public void openF2A2() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        String tag = F2A2.class.getName();
        FragmentTransaction addTransaction = transaction.add(
                R.id.frame_layout, F2A2.newInstance("",""), tag
        );

        addTransaction.addToBackStack(tag);
        addTransaction.commit();
    }

    @Override
    public void goToF1A2() {
        getSupportFragmentManager().popBackStack();
    }

    @Override
    public void closeActivity2() {
        finish();
    }

    @Override
    public void onBackPressed() {
        //moveTaskToBack(true);
        finishAffinity();
        finish();
    }
}