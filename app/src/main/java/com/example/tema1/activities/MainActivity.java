package com.example.tema1.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.example.tema1.R;
import com.example.tema1.interfaces.ActivityFragmentCommunication;

public class MainActivity extends AppCompatActivity implements ActivityFragmentCommunication {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void openSecondActivity() {
        Intent intent=new Intent(this,MainActivity2.class);
        startActivity(intent);
    }

    @Override
    public void replaceWithF3A2() {

    }

    @Override
    public void openF2A2() {

    }

    @Override
    public void goToF1A2() {

    }

    @Override
    public void closeActivity2() {

    }
}