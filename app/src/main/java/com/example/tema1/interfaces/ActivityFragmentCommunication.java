package com.example.tema1.interfaces;

public interface ActivityFragmentCommunication {

    void openSecondActivity();
    void replaceWithF3A2();
    void openF2A2();
    void goToF1A2();
    void closeActivity2();
}
